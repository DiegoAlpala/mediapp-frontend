import { Examen } from './../_model/examen';
import { Consulta } from './../_model/consulta';

export class ConsultaExamenDTO {
    consulta: Consulta
    listaExamen: Examen[]
}