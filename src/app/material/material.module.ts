import { MatPaginatorImpl } from './mat-paginator';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule, MatToolbarModule, MatButtonModule, MatMenuModule, MatIconModule, MatDividerModule, MatTableModule, MatInputModule, MatFormFieldModule, MatSortModule, MatPaginatorModule, MatCardModule, MatSnackBarModule, MatDialogModule, MatDatepickerModule, MatSelectModule, MatNativeDateModule, MAT_DATE_LOCALE, MatExpansionModule, MatAutocompleteModule, MatStepperModule, MatSlideToggleModule, MatGridListModule, MatPaginatorIntl, MatProgressBarModule, } from '@angular/material';


@NgModule({
   declarations: [],
   imports: [
      CommonModule,
      MatSidenavModule,
      MatToolbarModule,
      MatButtonModule,
      MatMenuModule,
      MatIconModule,
      MatDividerModule,
      MatTableModule,
      MatFormFieldModule,
      MatInputModule,
      MatSortModule,
      MatPaginatorModule,
      MatCardModule,
      MatSnackBarModule,
      MatDialogModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatSelectModule,
      MatExpansionModule,
      MatAutocompleteModule,
      MatStepperModule,
      MatSlideToggleModule,
      MatGridListModule,
      MatProgressBarModule
   ],
   exports: [
      MatSidenavModule,
      MatToolbarModule,
      MatButtonModule,
      MatMenuModule,
      MatIconModule,
      MatDividerModule,
      MatTableModule,
      MatFormFieldModule,
      MatInputModule,
      MatSortModule,
      MatPaginatorModule,
      MatCardModule,
      MatSnackBarModule,
      MatDialogModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatSelectModule,
      MatExpansionModule,
      MatAutocompleteModule,
      MatStepperModule,
      MatSlideToggleModule,
      MatGridListModule,
      MatProgressBarModule
   ],
   providers: [
      { provide: MatPaginatorIntl, useClass: MatPaginatorImpl },
      { provide: MAT_DATE_LOCALE, useValue: 'es-ES' }
   ]
})
export class MaterialModule { }
