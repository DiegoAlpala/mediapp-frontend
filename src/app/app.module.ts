import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MedicoDialogoComponent } from './pages/medico/medico-dialogo/medico-dialogo.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialComponent } from './pages/consulta/especial/especial.component';
import { WizardComponent } from './pages/wizard/wizard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BuscarComponent } from './pages/buscar/buscar.component';
import { BuscarDialogoComponent } from './pages/buscar/buscar-dialogo/buscar-dialogo.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
//libreria de pdf-Viewer
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { LoginComponent } from './pages/login/login.component';
import { environment } from 'src/environments/environment';
//Libreria para el manejo de JWT
import { JwtModule } from "@auth0/angular-jwt";
import { Not403Component } from './pages/not403/not403.component';
import { Not404Component } from './pages/not404/not404.component';
import { RecuperarComponent } from './pages/login/recuperar/recuperar.component';
import { TokenComponent } from './pages/login/recuperar/token/token.component';
import { ServerErrorsInterceptor } from './_shared/server-errors.interceptor';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { SignosComponent } from './pages/signos/signos.component';
import { SignosEdicionComponent } from './pages/signos/signos-edicion/signos-edicion.component';
import { RefreshTokenComponent } from './pages/refresh-token/refresh-token.component';
import { PacienteModalComponent } from './pages/paciente/paciente-modal/paciente-modal.component';

//Generamos la función para obtener el token almacenado en localsesionStorage
export function tokenGetter(){
   let tk = sessionStorage.getItem(environment.TOKEN_NAME);
   return tk != null ? tk : '';
}

@NgModule({
   declarations: [
      AppComponent,
      PacienteComponent,
      MedicoComponent,
      ExamenComponent,
      EspecialidadComponent,
      PacienteEdicionComponent,
      MedicoDialogoComponent,
      ExamenEdicionComponent,
      EspecialidadEdicionComponent,
      ConsultaComponent,
      EspecialComponent,
      WizardComponent,
      BuscarComponent,
      BuscarDialogoComponent,
      ReporteComponent,
      LoginComponent,
      Not403Component,
      Not404Component,
      RecuperarComponent,
      TokenComponent,
      PerfilComponent,
      SignosComponent,
      SignosEdicionComponent,
      RefreshTokenComponent,
      PacienteModalComponent
   ],
   /* Para usar dialogos en Angular 8.23 se necesita de la propiedad entryComponents que significa enbeber componentes en otros. Basta con
      llamar al componente que represnetara el dialogo*/
   entryComponents:[ MedicoDialogoComponent, BuscarDialogoComponent, RefreshTokenComponent, PacienteModalComponent],
   imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      MaterialModule,
      HttpClientModule,
      ReactiveFormsModule,
      FormsModule,
      FlexLayoutModule,
      PdfViewerModule,
      JwtModule.forRoot({
         config: {
           tokenGetter: tokenGetter,
           whitelistedDomains: ["localhost:8080"],
           blacklistedRoutes: ["http://localhost:8080:login/enviarcorreo"]
         }
       })
   ],
   providers: [
      {
         provide: HTTP_INTERCEPTORS,
         useClass: ServerErrorsInterceptor,
         multi: true,
       },
       { provide: LocationStrategy, useClass: HashLocationStrategy }
   ],
   bootstrap: [AppComponent]
})
export class AppModule { }
