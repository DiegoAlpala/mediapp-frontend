import { environment } from './../../environments/environment';
import { Subject } from 'rxjs';
import { Especialidad } from './../_model/especialidad';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class EspecialidadService {


    especialidadCambio = new Subject<Especialidad[]>();
    mensajeCambio = new Subject<string>();

    // la comilla invertida hace que escribas n linea
    url: string = `${environment.HOST}/especialidades`;
    constructor(
        private http: HttpClient
    ) { }

    listar() {
        return this.http.get<Especialidad[]>(this.url)
    }

    listarPorId(idEspecialidad: number) {
        return this.http.get<Especialidad>(`${this.url}/${idEspecialidad}`);
    }

    registrar(especialidad: Especialidad) {
        return this.http.post<Especialidad>(this.url, especialidad);
    }

    modificar(especialidad: Especialidad) {
        return this.http.put<Especialidad>(this.url, especialidad);
    }

    eliminar(idEspecialidad: number) {
        return this.http.delete(`${this.url}/${idEspecialidad}`);
    }
}
