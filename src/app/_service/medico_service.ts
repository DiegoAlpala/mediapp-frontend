import { Medico } from './../_model/medico';
import { environment } from './../../environments/environment';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

//Permitirá hacer la inyección de dependencias similar a la de Spring boot
@Injectable({
    providedIn: 'root'
})
export class MedicoService {

    medicoCambio = new Subject<Medico[]>();

    mensajeCambio = new Subject<string>();
    
    // la comilla invertida hace que escribas n linea
    url: string = `${environment.HOST}/medicos`;

    constructor(private http: HttpClient) { }

    listar() {
        return this.http.get<Medico[]>(this.url)
    }

    listarPorId(idMedico: number) {
        return this.http.get<Medico>(`${this.url}/${idMedico}`);
    }

    registrar(medico: Medico) {
        return this.http.post<Medico>(this.url, medico);
    }

    modificar(medico: Medico) {
        return this.http.put<Medico>(this.url, medico);
    }

    eliminar(idMedico: number) {
        return this.http.delete(`${this.url}/${idMedico}`);
    }
}