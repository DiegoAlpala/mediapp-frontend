import { ConsultaResumenDTO } from './../_dto/consultaResumenDTO';
import { Consulta } from './../_model/consulta';
import { FiltroConsultaDTO } from './../_dto/filtroConsultaDTO';
import { ConsultaExamenDTO } from './../_dto/consultaExamenDTO';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ConsultaService {

    url: string = `${environment.HOST}/consultas`;

    constructor(
        private http: HttpClient
    ) { }

    registrar(dto: ConsultaExamenDTO) {
        return this.http.post(this.url, dto);
    }

    buscar(filtroConsulta: FiltroConsultaDTO) {
        return this.http.post<Consulta[]>(`${this.url}/buscar`, filtroConsulta);
    }

    listarExamenPorConsulta(idConsulta : number){
        return this.http.get<ConsultaExamenDTO[]>(`${environment.HOST}/consultaexamenes/${idConsulta}`);
    }

    listarResumen(){
        return this.http.get<ConsultaResumenDTO[]>(`${this.url}/listarResumen`)
    }

    generarReporte(){
        return this.http.get(`${this.url}/generarReporte`,{
            responseType:'blob'
        })
    }

    guardarArchivo(data : File){
        let formData = new FormData();
        formData.append('adjunto', data);
        return this.http.post(`${this.url}/guardarArchivo`, formData);
    }

    leerArchivo(id : number){
        return this.http.get(`${this.url}/leerArchivo/${id}`,{
            responseType:'blob'
        })
    }
}
