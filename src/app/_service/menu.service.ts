import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Menu } from '../_model/menu';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    menuCambio = new Subject<Menu[]>();

    url: string = `${environment.HOST}/menus`;

    constructor(private http: HttpClient) { }

    /* Estos 2 metodos usa la forma manuala de agregar el token a la petición. */
    listar() {
        let token = sessionStorage.getItem(environment.TOKEN_NAME);
        return this.http.get<Menu[]>(`${this.url}`, {
            headers: new HttpHeaders().set('Authorization', `bearer ${token}`).set('Content-Type', 'application/json')
        })
    }

    listarPorUsuario(usuario: string) {
        let token = sessionStorage.getItem(environment.TOKEN_NAME);
        return this.http.post<Menu[]>(`${this.url}/usuario`,usuario, {
            headers: new HttpHeaders().set('Authorization', `bearer ${token}`).set('Content-Type', 'application/json')
        })
    }
}
