import { Paciente } from './../_model/paciente';
import { environment } from './../../environments/environment';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

//Permitirá hacer la inyección de dependencias similar a la de Spring boot
@Injectable({
    providedIn: 'root'
})
export class PacienteService {

    pacienteCambio = new Subject<Paciente[]>();

    mensajeCambio = new Subject<string>();

    modalEdicionPacienteCambio = new Subject<boolean>();
    
    // la comilla invertida hace que escribas n linea
    url: string = `${environment.HOST}/pacientes`;

    constructor(private http: HttpClient) { }

    listar() {
        return this.http.get<Paciente[]>(this.url)
    }

    listarPorId(idPaciente: number) {
        return this.http.get<Paciente>(`${this.url}/${idPaciente}`);
    }

    registrar(paciente: Paciente) {
        return this.http.post<Paciente>(this.url, paciente);
    }

    modificar(paciente: Paciente) {
        return this.http.put<Paciente>(this.url, paciente);
    }

    eliminar(idPaciente: number) {
        return this.http.delete(`${this.url}/${idPaciente}`);
    }

    listarPageable(p: number, s: number){
        return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
    }
}