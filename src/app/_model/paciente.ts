export class Paciente{

   idPaciente: number;

   nombres: string;

   apellidos : string;

   dni: string;

   email: string;

   direccion : string

   telefono : string
}