import { Router } from '@angular/router';
import { LoginService } from './../../_service/login.service';
import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/_service/menu.service';
import '../../../assets/login-animation.js';
import { environment } from 'src/environments/environment';
//librería para obtener la información del token como usuario.
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    usuario : string;
    clave : string;
    mensaje : string;
    error : string;

    constructor(
        private loginService : LoginService,
        private menuService : MenuService,
        private router : Router
    ) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        (window as any).initialize();
    }

    iniciarSesion(){
        console.log(this.usuario);
        console.log(this.clave);
        this.loginService.login(this.usuario, this.clave).subscribe(data => {
            sessionStorage.setItem(environment.TOKEN_NAME, data.access_token);

            const helper = new JwtHelperService();
            const decodedToken = helper.decodeToken(data.access_token);
            console.log(decodedToken);

            this.menuService.listarPorUsuario(decodedToken.user_name).subscribe(data => {
                //console.log(data);
                this.menuService.menuCambio.next(data);
            });
            this.router.navigate(['paciente']);
        });
    }
}
