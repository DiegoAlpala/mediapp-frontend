import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Especialidad } from './../../_model/especialidad';
import { EspecialidadService } from './../../_service/especialidad.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-especialidad',
    templateUrl: './especialidad.component.html',
    styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

    displayedColumns = ['idEspecialidad', 'nombre', 'acciones']
    dataSource: MatTableDataSource<Especialidad>;
    @ViewChild(MatSort, { static: true }) sort: MatSort //permite tener una referencia a nivel de codigo de lo que está en el html
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator

    /* La Variable route de tipo ActivatedRoute y que es PUBLIC permitirá usar en el html */
    constructor(
        private especialidadService: EspecialidadService,
        private snackBar: MatSnackBar,
        public route : ActivatedRoute // La Variable route de tipo ActivatedRoute y que es PUBLIC permitirá usar en el html 
    ) { }

    ngOnInit() {
        this.especialidadService.mensajeCambio.subscribe(data =>{
            this.snackBar.open(data, 'AVISO', {
                duration : 2000
            })
        });
        /* Variable Reactiva se ejecuta cuando le hacen un next a esa variable  desde cualquier componente */
        this.especialidadService.especialidadCambio.subscribe(data =>{
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        })

        this.especialidadService.listar().subscribe(data => {
            console.log(data);
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        });

        /* ============ Uso del FORKJOIN 1 ESCENARIO  ==================*/
       /*  let array = [];
        for (let i = 0; i < 3; i++) {
            let observable = this.especialidadService.listar();
            array.push(observable);
        }

        forkJoin(array).subscribe(data=>{
            console.log(data);
        })
 */
        /* Uso del FORKJOIN 2 ESCENARIO */
       /*  let obs = this.especialidadService.listar();
        let obs2 = this.especialidadService.listar();
        let obs3 = this.especialidadService.listar();

        forkJoin(obs, obs2, obs3).subscribe(data =>{
            console.log(data);
        }) */

    }

    filtrar(e: string) {
        this.dataSource.filter = e.trim().toLowerCase();
    }

    eliminar(idPaciente: number) {
        this.especialidadService.eliminar(idPaciente).subscribe(() => {
            this.especialidadService.listar().subscribe(data => {
                this.especialidadService.especialidadCambio.next(data);
                this.especialidadService.mensajeCambio.next('Especialidad Eliminado');
            });
        });
    }


}
