import { switchMap } from 'rxjs/operators';
import { EspecialidadService } from './../../../_service/especialidad.service';
import { Especialidad } from './../../../_model/especialidad';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
    selector: 'app-especialidad-edicion',
    templateUrl: './especialidad-edicion.component.html',
    styleUrls: ['./especialidad-edicion.component.css']
})
export class EspecialidadEdicionComponent implements OnInit {

    form: FormGroup;
    id: number;
    edicion: boolean;
    especialidad: Especialidad;

    /* ActivatedRoute: Sirve para identificar la URL actual */
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private especialidadService: EspecialidadService

    ) { }


    ngOnInit() {
        this.especialidad = new Especialidad();

        this.form = new FormGroup({
            'id': new FormControl(0),
            'nombre': new FormControl(''),
        });

        /* Params es una propiedad de Route y devuelve los parametros enviados en la URL  */
        this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            this.edicion = params['id'] != null;
            this.initForm();
        })
    }

    operar(){
        debugger
        this.especialidad.idEspecialidad = this.form.value['id'];
        this.especialidad.nombre = this.form.value['nombre'];

        if(this.especialidad  != null && this.especialidad.idEspecialidad >0){
            // BUENA PRÁCTICA
            this.especialidadService.modificar(this.especialidad).pipe(switchMap(()=> {
                return this.especialidadService.listar();
            })).subscribe(data =>{
                this.especialidadService.especialidadCambio.next(data);
                this.especialidadService.mensajeCambio.next('ESPECIALIDAD MODIFICADO')
            })
        }else{
            //PRACTICA COMÚN
            this.especialidadService.registrar(this.especialidad).subscribe(()=>{
                this.especialidadService.listar().subscribe(data=>{
                    this.especialidadService.especialidadCambio.next(data);
                    this.especialidadService.mensajeCambio.next('ESPECIALIDAD REGISTRADO');
                    
                })
            })

        }

         /* router de tipo ROUTER:  Permite navegar a donde necesitemos */
         this.router.navigate(['especialidad']);
    }

    initForm() {
        if (this.edicion) {
            this.especialidadService.listarPorId(this.id).subscribe(data => {
                this.form = new FormGroup({
                    'id': new FormControl(data.idEspecialidad),
                    'nombre': new FormControl(data.nombre),
                });
            });
        }
    }

}
