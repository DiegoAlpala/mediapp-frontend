import { PacienteService } from './../../_service/paciente_service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Paciente } from 'src/app/_model/paciente';

@Component({
    selector: 'app-paciente',
    templateUrl: './paciente.component.html',
    styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

    displayedColumns = ['idPaciente', 'nombres', 'apellidos', 'acciones']
    dataSource: MatTableDataSource<Paciente>;
    @ViewChild(MatSort, { static: true }) sort: MatSort //permite tener una referencia a nivel de codigo de lo que está en el html
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator

    cantidad : number =0;

    constructor(
        private pacienteService: PacienteService,
        private snackBar : MatSnackBar
        ) { }

    ngOnInit() {

        this.pacienteService.mensajeCambio.subscribe(data =>{
            this.snackBar.open(data, 'AVISO', {
                duration : 2000
            })
        });
        /* Variable Reactiva se ejecuta cuando le hacen un next a esa variable  desde cualquier componente */
        this.pacienteService.pacienteCambio.subscribe(data =>{
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        })

       /*  this.pacienteService.listar().subscribe(data => {
            console.log(data);
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        }); */

        this.pacienteService.listarPageable(0,10).subscribe(data => {
            console.log(data);
            this.cantidad = data.totalElements;
            this.dataSource = new MatTableDataSource(data.content);
            this.dataSource.sort = this.sort;
            //this.dataSource.paginator = this.paginator;
        });

        this.pacienteService.listarPageable
    }

    filtrar(e: string) {
        this.dataSource.filter = e.trim().toLowerCase();
    }

    eliminar(idPaciente: number) {
        this.pacienteService.eliminar(idPaciente).subscribe(() => {
            this.pacienteService.listar().subscribe(data => {
                this.pacienteService.pacienteCambio.next(data);
                this.pacienteService.mensajeCambio.next('Paciente eliminado');
            });
        });
    }

    mostrarMas(e: any){
        this.pacienteService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
            console.log(data);
            this.cantidad = data.totalElements;
            this.dataSource = new MatTableDataSource(data.content);
            this.dataSource.sort = this.sort;
        })
    }

}
