import { Paciente } from './../../../_model/paciente';
import { PacienteService } from './../../../_service/paciente_service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';


@Component({
    selector: 'app-paciente-edicion',
    templateUrl: './paciente-edicion.component.html',
    styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

    @Input()
    nombreComponentePadre : String;
    form: FormGroup;
    id: number;
    edicion: boolean;

    /* ActivatedRoute: Sirve para identificar la URL actual */
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private pacienteService: PacienteService) { }

    ngOnInit() {
        this.form = new FormGroup({
            'id': new FormControl(0),
            'nombres': new FormControl('', [Validators.required, Validators.minLength(3)]), //Validadores para formularios.
            'apellidos': new FormControl('', Validators.required),
            'dni': new FormControl(''),
            'email': new FormControl('', Validators.email),
            'telefono': new FormControl(''),
            'direccion': new FormControl(''),
        });

        /* Params es una propiedad de Route y devuelve los parametros enviados en la URL  */
        this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            this.edicion = params['id'] != null;
            this.initForm();
        })
    }

    operar() {
        debugger
        
        if(this.form.invalid){
            return;
        }
        
        let paciente = new Paciente();        
        paciente.idPaciente = this.form.value['id'];
        paciente.nombres = this.form.value['nombres'];
        paciente.apellidos = this.form.value['apellidos'];
        paciente.dni = this.form.value['dni'];
        paciente.email = this.form.value['email'];
        paciente.telefono = this.form.value['telefono'];
        paciente.direccion = this.form.value['direccion'];

        if (this.edicion) {
            this.pacienteService.modificar(paciente).subscribe(() =>{
                this.pacienteService.listar().subscribe(data => {
                    this.pacienteService.pacienteCambio.next(data);
                    this.pacienteService.mensajeCambio.next('Paciente modificado');
                });
            });
        } else {
            this.pacienteService.registrar(paciente).subscribe(() =>{
                this.pacienteService.listar().subscribe(data => {
                    this.pacienteService.pacienteCambio.next(data);
                    this.pacienteService.mensajeCambio.next('Paciente creado');
                    this.pacienteService.modalEdicionPacienteCambio.next(true);
                });
            });
        }

        /* router de tipo ROUTER:  Permite navegar a donde necesitemos */
        if(this.nombreComponentePadre === "paciente-modal"){
            this.pacienteService.modalEdicionPacienteCambio.next(true);
            this.router.navigate(['signos/nuevo']);
        }else{
            this.router.navigate(['paciente']);
        }
         
    }

    initForm() {
        if (this.edicion) {
            this.pacienteService.listarPorId(this.id).subscribe(data => {
                this.form = new FormGroup({
                    'id': new FormControl(data.idPaciente),
                    'nombres': new FormControl(data.nombres),
                    'apellidos': new FormControl(data.apellidos),
                    'dni': new FormControl(data.dni),
                    'email': new FormControl(data.email),
                    'telefono': new FormControl(data.telefono),
                    'direccion': new FormControl(data.direccion),
                });


            });
        }
    }

    cancelar(){
        if(this.nombreComponentePadre === "paciente-modal"){
            this.pacienteService.modalEdicionPacienteCambio.next(true);
            this.router.navigate(['signos/nuevo']);
            console.log("navega a signos/nuevo");
        }else{
            this.router.navigate(['paciente']);
        }
        
    }

    /* Función de solo lectura qu se usa para obtner los mensajes de validación*/
    get f(){return this.form.controls; }

}
