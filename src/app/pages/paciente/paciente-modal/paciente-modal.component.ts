import { Component, OnInit, Inject } from '@angular/core';
import { PacienteEdicionComponent } from '../paciente-edicion/paciente-edicion.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente_service';

@Component({
    selector: 'app-paciente-modal',
    templateUrl: './paciente-modal.component.html',
    styleUrls: ['./paciente-modal.component.css']
})
export class PacienteModalComponent implements OnInit {

    constructor(
        private dialogRef: MatDialogRef<PacienteModalComponent>,
        private pacienteService: PacienteService,
    ) { }

    ngOnInit() {
        this.pacienteService.pacienteCambio.subscribe(data => {
            this.dialogRef.close();
        });

        this.pacienteService.modalEdicionPacienteCambio.subscribe(data=>{
            this.dialogRef.close();
        })
    }


}
