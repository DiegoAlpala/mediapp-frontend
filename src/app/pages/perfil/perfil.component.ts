import { Component, OnInit } from '@angular/core';
import { Rol } from 'src/app/_model/rol';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { decode } from 'punycode';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

    usuario: string;
    roles: Rol[];

    constructor() { }

    ngOnInit() {
        this.obtenerUsuarioLogin();
    }

    obtenerUsuarioLogin(){
        let token = sessionStorage.getItem(environment.TOKEN_NAME);
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token);
        this.usuario=decodedToken.user_name;
        this.roles = decodedToken.authorities;
        console.log(decodedToken);
    }

}
