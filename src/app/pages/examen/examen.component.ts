import { ExamenService } from './../../_service/examen.service';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Examen } from './../../_model/examen';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
    selector: 'app-examen',
    templateUrl: './examen.component.html',
    styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {

    displayedColumns = ['idExamen', 'nombre', 'descripcion', 'acciones']
    dataSource: MatTableDataSource<Examen>;
    @ViewChild(MatSort, { static: true }) sort: MatSort //permite tener una referencia a nivel de codigo de lo que está en el html
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator

    constructor(
        private examenService: ExamenService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit() {
        this.examenService.mensajeCambio.subscribe(data =>{
            this.snackBar.open(data, 'AVISO', {
                duration : 2000
            })
        });
        /* Variable Reactiva se ejecuta cuando le hacen un next a esa variable  desde cualquier componente */
        this.examenService.examenCambio.subscribe(data =>{
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        })

        this.examenService.listar().subscribe(data => {
            console.log(data);
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        });
    }

    filtrar(e: string) {
        this.dataSource.filter = e.trim().toLowerCase();
    }

    eliminar(idPaciente: number) {
        this.examenService.eliminar(idPaciente).subscribe(() => {
            this.examenService.listar().subscribe(data => {
                this.examenService.examenCambio.next(data);
                this.examenService.mensajeCambio.next('Exámen eliminado');
            });
        });
    }

}
