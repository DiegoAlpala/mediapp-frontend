import { switchMap } from 'rxjs/operators';
import { Examen } from './../../../_model/examen';
import { ExamenService } from './../../../_service/examen.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
    selector: 'app-examen-edicion',
    templateUrl: './examen-edicion.component.html',
    styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {

    form: FormGroup;
    id: number;
    edicion: boolean;
    examen: Examen;

    /* ActivatedRoute: Sirve para identificar la URL actual */
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private examenService : ExamenService
        
    ) { }

    ngOnInit() {
        this.examen = new Examen();

        this.form = new FormGroup({
            'id': new FormControl(0),
            'nombre': new FormControl(''),
            'descripcion': new FormControl('')
        });

         /* Params es una propiedad de Route y devuelve los parametros enviados en la URL  */
         this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            this.edicion = params['id'] != null;
            this.initForm();
        })
    }

    operar(){
        debugger
        this.examen.idExamen = this.form.value['id'];
        this.examen.nombre = this.form.value['nombre'];
        this.examen.descripcion = this.form.value['descripcion'];

        if(this.examen  != null && this.examen.idExamen >0){
            // BUENA PRÁCTICA
            this.examenService.modificar(this.examen).pipe(switchMap(()=> {
                return this.examenService.listar();
            })).subscribe(data =>{
                this.examenService.examenCambio.next(data);
                this.examenService.mensajeCambio.next('EXÁMEN MODIFICADO')
            })
        }else{
            //PRACTICA COMÚN
            this.examenService.registrar(this.examen).subscribe(()=>{
                this.examenService.listar().subscribe(data=>{
                    this.examenService.examenCambio.next(data);
                    this.examenService.mensajeCambio.next('EXÁMEN REGISTRADO');
                    
                })
            })

        }

         /* router de tipo ROUTER:  Permite navegar a donde necesitemos */
         this.router.navigate(['examen']);
    }

    initForm() {
        if (this.edicion) {
            this.examenService.listarPorId(this.id).subscribe(data => {
                this.form = new FormGroup({
                    'id': new FormControl(data.idExamen),
                    'nombre': new FormControl(data.nombre),
                    'descripcion': new FormControl(data.descripcion),
                });


            });
        }
    }

}
