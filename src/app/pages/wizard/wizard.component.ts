import { ConsultaExamenDTO } from './../../_dto/consultaExamenDTO';
import { Consulta } from './../../_model/consulta';
import { ConsultaService } from './../../_service/consulta.service';
import { ExamenService } from './../../_service/examen.service';
import { EspecialidadService } from './../../_service/especialidad.service';
import { MedicoService } from './../../_service/medico_service';
import { PacienteService } from './../../_service/paciente_service';
import { DetalleConsulta } from './../../_model/detalleConsulta';
import { Examen } from './../../_model/examen';
import { Especialidad } from './../../_model/especialidad';
import { Medico } from './../../_model/medico';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { MatSnackBar, MatStepper } from '@angular/material';
import * as moment from 'moment';

@Component({
    selector: 'app-wizard',
    templateUrl: './wizard.component.html',
    styleUrls: ['./wizard.component.css']
})
export class WizardComponent implements OnInit {

    isLienar = false;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;

    pacientes: Paciente[];
    medicos: Medico[];
    especialidades: Especialidad[];
    examenes: Examen[];

    maxFecha: Date = new Date();
    fechaSeleccionada: Date = new Date();

    diagnostico: string;
    tratamiento: string;
    mensaje: string;

    detalleConsulta: DetalleConsulta[] = [];
    examenesSeleccionados: Examen[] = [];

    pacienteSeleccionado: Paciente;
    especialidadSeleccionado: Especialidad;
    examenSeleccionado: Examen;
    medicoSeleccionado: Medico;

    consultorios: number[] = [];
    consultorioSeleccionado: number = 0;

    @ViewChild('stepper', {static: true}) stepper : MatStepper;

    constructor(
        private pacinteService: PacienteService,
        private medicoService: MedicoService,
        private especialidadService: EspecialidadService,
        private examenService: ExamenService,
        private consultaService: ConsultaService,
        private snakBar: MatSnackBar,
        private formBuilder: FormBuilder // Sirve para agrupar formularios.
    ) { }

    ngOnInit() {
        this.firstFormGroup = this.formBuilder.group({
            firstCtrl: ['', Validators.required],
            'pacienteSeleccionado': new FormControl(),
            'fecha': new FormControl(new Date()),
            'diagnostico': new FormControl(''),
            'tratamiento': new FormControl('')
        });

        this.secondFormGroup = this.formBuilder.group({
            secondCtrl: ['', Validators.required]
        })

        this.listarPacientes();
        this.listarMedicos();
        this.listarExamenes();
        this.listarEspecialidades();
        this.listarConsultorios();
    }

    listarConsultorios() {
        for (let i = 1; i <= 20; i++) {
            this.consultorios.push(i);
        }
    }

    listarPacientes() {
        this.pacinteService.listar().subscribe(data => {
            this.pacientes = data;
        })
    }

    listarMedicos() {
        this.medicoService.listar().subscribe(data => {
            this.medicos = data;
        })
    }

    listarExamenes() {
        this.examenService.listar().subscribe(data => {
            this.examenes = data;
        })
    }

    listarEspecialidades() {
        this.especialidadService.listar().subscribe(data => {
            this.especialidades = data;
        })
    }

    registrar(){
        let consulta = new Consulta();
        consulta.paciente = this.pacienteSeleccionado;
        consulta.medico = this. medicoSeleccionado;
        consulta.especialidad = this.especialidadSeleccionado;
        consulta.fecha = moment().format('YYYY-MM-DDTHH:mm:ss');
        consulta.detalleConsulta = this.detalleConsulta;
        consulta.numeroConsultorio = `C${this.consultorioSeleccionado}`

        let consultaExamenDTO = new ConsultaExamenDTO();
        consultaExamenDTO.consulta = consulta;
        consultaExamenDTO.listaExamen = this.examenesSeleccionados;

        this.consultaService.registrar(consultaExamenDTO).subscribe(()=>{
            this.snakBar.open('Consulta registrada', 'AVISO', { duration: 2000 });
            
            setTimeout(()=> {
                this.limpiarControles();
            }, 2000)
        })
    }

    agregarDetalle() {
        if (this.diagnostico != null && this.tratamiento != null) {
            let det = new DetalleConsulta();
            det.diagnostico = this.diagnostico;
            det.tratamiento = this.tratamiento;
            this.detalleConsulta.push(det);

            this.diagnostico = null;
            this.tratamiento = null;
        }
    }

    removerDiagnostico(i: number) {
        this.detalleConsulta.splice(i, 1);
    }

    agregarExamen() {
        if (this.examenSeleccionado != null) {
            debugger
            let cont: number = 0;
            for (let i = 0; i < this.examenesSeleccionados.length; i++) {
                let examen = this.examenesSeleccionados[i];
                if (examen.idExamen === this.examenSeleccionado.idExamen) {
                    cont++;
                    break;
                }
            }

            if (cont > 0) {
                this.mensaje = 'El exámen ya se ecuentra en la lista ';
                this.snakBar.open(this.mensaje, 'AVISO', { duration: 2000 });
            } else {
                let exa = new Examen();
                exa.idExamen = this.examenSeleccionado.idExamen;

                this.examenesSeleccionados.push(this.obtenerExamenOfLista(exa.idExamen));
            }

        } else {
            this.mensaje = 'Debe seleccionar un exámen ';
            this.snakBar.open(this.mensaje, 'AVISO', { duration: 2000 });
        }
    }

    obtenerExamenOfLista(idExamen: number) {
        debugger
        let examenAuxiliar = new Examen();
        this.examenes.map(function (e) {
            if (e.idExamen === idExamen) {
                examenAuxiliar = e;
            }
        });
        return examenAuxiliar;
    }

    removerExamen(index: number) {
        this.examenesSeleccionados.splice(index, 1);
    }

    seleccionarConsultorio(c: number) {
        this.consultorioSeleccionado = c;
    }

    seleccionarMedico(m: Medico) {
        this.medicoSeleccionado = m;
    }

    estadoBotonRegistrar(){
        return (this.especialidadSeleccionado === null || this.pacienteSeleccionado === null  || this.medicoSeleccionado === null || this.detalleConsulta.length === 0 )
    }

    limpiarControles(){
        this.detalleConsulta = [];
        this.examenesSeleccionados = [];
        this.diagnostico= null;
        this.tratamiento= null;
        this.especialidadSeleccionado =null;
        this.pacienteSeleccionado = null;
        this.medicoSeleccionado =null;
        this.examenSeleccionado = null;
        this.fechaSeleccionada = new Date();
        this.mensaje = '';
        this.stepper.reset();
    }

}
