import { ConsultaService } from './../../../_service/consulta.service';
import { ConsultaExamenDTO } from './../../../_dto/consultaExamenDTO';
import { Consulta } from './../../../_model/consulta';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-buscar-dialogo',
    templateUrl: './buscar-dialogo.component.html',
    styleUrls: ['./buscar-dialogo.component.css']
})
export class BuscarDialogoComponent implements OnInit {

    consulta: Consulta;
    examenes : ConsultaExamenDTO[];

    constructor(
        private dialogoRef : MatDialogRef<BuscarDialogoComponent>,
        @Inject(MAT_DIALOG_DATA) private data: Consulta,
        private consultaService : ConsultaService
    ) { }

    ngOnInit() {
        this.consulta = this.data;
        this.listarExamenes();
    }

    listarExamenes(){
        this.consultaService.listarExamenPorConsulta(this.consulta.idConsulta).subscribe(data => {
           console.log(data);
            this.examenes = data;
        })
    }

    cancelar(){
        this.dialogoRef.close();
    }

}
