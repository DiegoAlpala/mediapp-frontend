import { ConsultaService } from './../../_service/consulta.service';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-reporte',
    templateUrl: './reporte.component.html',
    styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {

    tipo: string;
    chart: any;
    pdfSRC: string;

    archivosSeleccionados: FileList;
    archivoSeleccionado: File;
    nombreArchivo: string;

    imagenData: any;
    imagenEstado: boolean;

    constructor(
        private consultaService: ConsultaService,
        //Permite enapsular al base 64 por seguridad.
        private sanitization : DomSanitizer 
    ) { }

    ngOnInit() {
        this.tipo = 'line';
        this.dibujar();

        this.consultaService.leerArchivo(1).subscribe(data => this.convertir(data));

    }
    convertir(data: any) {
        let reader = new FileReader();
        reader.readAsDataURL(data);
        reader.onloadend = () => {
            let x = reader.result;
            //console.log(x); //base64
            this.setear(x);
        }
    }

    setear(data : any){
        //Para asegurar la data y poder observar la imagen.
        this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(data);
        this.imagenEstado = true;
    }

    cambiar(tipo: string) {
        this.tipo = tipo;
        // para arreglar el bug de la libreria que superpone los gráfico el uno con el otro.
        //Se tiene que destriir el objecto
        if (this.chart != null) {
            this.chart.destroy();
        }
        this.dibujar();
    }

    dibujar() {
        this.consultaService.listarResumen().subscribe(data => {
            console.log(data);
            let cantidades = data.map(x => x.cantidad);
            let fechas = data.map(x => x.fecha);

            this.chart = new Chart('canvas', {
                type: this.tipo,
                data: {
                    labels: fechas,
                    datasets: [{
                        label: 'Cantidad',
                        data: cantidades,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            })
        })
    }

    generarReporte() {
        this.consultaService.generarReporte().subscribe(data => {
            let reader = new FileReader();
            reader.onload = (e: any) => {
                this.pdfSRC = e.target.result;
                console.log(this.pdfSRC);
            }
            reader.readAsArrayBuffer(data);
        })
    }

    descargarReporte() {
        this.consultaService.generarReporte().subscribe(data => {
            const url = window.URL.createObjectURL(data);
            const a = document.createElement('a');
            a.setAttribute('style', 'display:none');
            document.body.appendChild(a);
            a.href = url;
            a.download = 'reporte.pdf';
            a.click();

        });
    }

    seleccionarArchivo(e: any) {
        this.nombreArchivo = e.target.files[0].name;
        this.archivosSeleccionados = e.target.files;
    }

    subirArchivo() {
        this.archivoSeleccionado = this.archivosSeleccionados.item(0);

        this.consultaService.guardarArchivo(this.archivoSeleccionado).subscribe(data => console.log(data));
    }

    accionImagen(accion : string){
        if(accion == "M"){
            this.imagenEstado = true;
        }else{
            this.imagenEstado = false;
        }
    }

}
