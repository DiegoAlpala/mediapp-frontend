import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialogConfig } from '@angular/material';
import { AppComponent } from 'src/app/app.component';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
    selector: 'app-refresh-token',
    templateUrl: './refresh-token.component.html',
    styleUrls: ['./refresh-token.component.css']
})
export class RefreshTokenComponent implements OnInit {

    constructor(
        private dialogRef: MatDialogRef<RefreshTokenComponent>,
        private route: ActivatedRoute,
        private route2 : Router
    ) { }

    ngOnInit() {
        
    }

    cancelar() {
        this.dialogRef.close();
    }

    operar(){
        debugger
        console.log(this.route.children);
        this.route.params.subscribe((params: Params) => {
            console.log(params);
        });

        this.route2.events.subscribe((url:any) => console.log(url));
        
    }

}
