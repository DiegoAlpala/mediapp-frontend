import { MedicoDialogoComponent } from './medico-dialogo/medico-dialogo.component';
import { MedicoService } from './../../_service/medico_service';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar, MatDialog } from '@angular/material';
import { Medico } from './../../_model/medico';
import { Component, OnInit, ViewChild } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'app-medico',
    templateUrl: './medico.component.html',
    styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

    displayedColumns = ['idMedico', 'nombres', 'apellidos', 'acciones']
    dataSource: MatTableDataSource<Medico>;
    @ViewChild(MatSort, { static: true }) sort: MatSort //permite tener una referencia a nivel de codigo de lo que está en el html
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator

    constructor(
        private snackBar: MatSnackBar,
        private medicoService: MedicoService,
        private dialog: MatDialog
    ) { }

    ngOnInit() {

        this.medicoService.medicoCambio.subscribe(data => {
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        });

        this.medicoService.mensajeCambio.subscribe(data => {
            this.snackBar.open(data, 'AVISO', {
                duration: 2000
            })
        })

        this.medicoService.listar().subscribe(data => {
            console.log(data);
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        });

    }

    filtrar(e: string) {
        this.dataSource.filter = e.trim().toLowerCase();
    }

    /* Para asignar que un campo es opcional se debe incluir el signo: ? */
    abrirDialogo(medico? : Medico) {
        let med = medico!= null ? medico: new Medico();
        this.dialog.open(MedicoDialogoComponent, {
            width: '350px',
            data: med
        })
    }

    eliminar(idMedico: number) {
        this.medicoService.eliminar(idMedico).subscribe();
    }

}
