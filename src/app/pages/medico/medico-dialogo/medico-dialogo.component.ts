import { MedicoService } from './../../../_service/medico_service';
import { Medico } from './../../../_model/medico';
import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { switchMap } from 'rxjs/operators'

@Component({
    selector: 'app-medico-dialogo',
    templateUrl: './medico-dialogo.component.html',
    styleUrls: ['./medico-dialogo.component.css']
})
export class MedicoDialogoComponent implements OnInit {

    private medico: Medico;
    constructor(
        private dialogRef: MatDialogRef<MedicoDialogoComponent>,
        @Inject(MAT_DIALOG_DATA) private data: Medico,
        private medicoService: MedicoService
    ) { }

    ngOnInit() {
        this.medico = new Medico();
        this.medico.idMedico = this.data.idMedico;
        this.medico.nombres = this.data.nombres;
        this.medico.apellidos = this.data.apellidos;
        this.medico.cmp = this.data.cmp;
        this.medico.fotoUrl = this.data.fotoUrl;

    }

    cancelar() {
        this.dialogRef.close();
    }

    operar() {
        if (this.medico != null && this.medico.idMedico > 0) {
            //Modificar
            // BUENA PRÁCTICA
            // función PIPE : Enlazar un observable con otro observable switchMap: hacer otra petitcion a otro elemento observable
            this.medicoService.modificar(this.medico).pipe(switchMap(() => {
                return this.medicoService.listar();
            })).subscribe(data => {
                this.medicoService.medicoCambio.next(data);
                this.medicoService.mensajeCambio.next('MODIFICADO')
            })
        } else {
            //Registrar
            //Práctica común
            this.medicoService.registrar(this.medico).subscribe(() => {
                this.medicoService.listar().subscribe(data => {
                    this.medicoService.medicoCambio.next(data);
                    this.medicoService.mensajeCambio.next('REGISTRADO')
                })
            });
        }
        this.dialogRef.close();
    }

}
