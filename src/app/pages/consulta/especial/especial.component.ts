import { ConsultaExamenDTO } from './../../../_dto/consultaExamenDTO';
import { Consulta } from './../../../_model/consulta';
import { ConsultaService } from './../../../_service/consulta.service';
import { ExamenService } from './../../../_service/examen.service';
import { EspecialidadService } from './../../../_service/especialidad.service';
import { MedicoService } from './../../../_service/medico_service';
import { DetalleConsulta } from './../../../_model/detalleConsulta';
import { Examen } from './../../../_model/examen';
import { Especialidad } from './../../../_model/especialidad';
import { Medico } from './../../../_model/medico';
import { PacienteService } from './../../../_service/paciente_service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-especial',
    templateUrl: './especial.component.html',
    styleUrls: ['./especial.component.css']
})
export class EspecialComponent implements OnInit {

    form: FormGroup;

    pacientes: Paciente[];
    medicos: Medico[];
    especialidades: Especialidad[];
    examenes: Examen[];

    maxFecha: Date = new Date();

    diagnostico: string;
    tratamiento: string;
    mensaje: string;

    detalleConsulta: DetalleConsulta[] = [];
    examenesSeleccionados: Examen[] = [];

    pacienteSeleccionado: Paciente;
    especialidadSeleccionado: Especialidad;
    examenSeleccionado: Examen;
    medicoSeleccionado: Medico;

    //ÚTILES PARA EL AUTOCOMPLETE
    myControlPaciente: FormControl = new FormControl();
    myControlMedico: FormControl = new FormControl();

    pacientesFiltrados: Observable<any[]>;
    medicosFiltrados: Observable<any[]>;

    constructor(
        private pacienteService: PacienteService,
        private medicoService: MedicoService,
        private especialidadService: EspecialidadService,
        private examenService: ExamenService,
        private consultaService: ConsultaService,
        private snakBar: MatSnackBar

    ) { }

    ngOnInit() {
        this.form = new FormGroup({
            'paciente': this.myControlPaciente,
            'especialidad': new FormControl(),
            'medico': this.myControlMedico,
            'fecha': new FormControl(new Date()),
            'diagnostico': new FormControl(''),
            'tratamiento': new FormControl(''),
        });

        this.listarPacientes();
        this.listarMedicos();
        this.listarExamenes();
        this.listarEspecialidades();

        /* Como el método: valueChanges devuleve un Observable . para manipular dicho objejo debemos usar PIPE */
        this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
        this.medicosFiltrados = this.myControlMedico.valueChanges.pipe(map(val => this.filtrarMedicos(val)));

    }

    filtrarPacientes(val: any) {
        console.log(val);
        if (val != null && val.idPaciente > 0) {
            return this.pacientes.filter(option =>
                option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.toLowerCase().includes(val.dni.toLowerCase()));
        } else {
            return this.pacientes.filter(option =>
                option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.toLowerCase().includes(val.toLowerCase()));
        }

    }
    filtrarMedicos(val: any) {
        console.log(val);
        if (val != null && val.idMedico > 0) {
            return this.medicos.filter(option =>
                option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.cmp.toLowerCase().includes(val.cmp.toLowerCase()));
        } else {
            return this.medicos.filter(option =>
                option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.cmp.toLowerCase().includes(val.toLowerCase()));
        }

    }

    /* Método para devolver lo que se quiere ver en la interfaz cuando se selecciona. */
    mostrarPaciente(val: Paciente) {
        return val ? `${val.nombres} ${val.apellidos}` : val;
    }
     
    /* Método para devolver lo que se quiere ver en la interfaz cuando se selecciona. */
      mostrarMedico(val: Medico) {
        return val ? `${val.nombres} ${val.apellidos}` : val;
    }
    
    seleccionarPaciente(e: any){
        this.pacienteSeleccionado = e.option.value;
    }

    seleccionarMedico(e: any){
        this.medicoSeleccionado = e.option.value;
    }


    listarPacientes() {
        this.pacienteService.listar().subscribe(data => {
            this.pacientes = data;
        })
    }

    listarMedicos() {
        this.medicoService.listar().subscribe(data => {
            this.medicos = data;
        })
    }

    listarExamenes() {
        this.examenService.listar().subscribe(data => {
            this.examenes = data;
        })
    }

    listarEspecialidades() {
        this.especialidadService.listar().subscribe(data => {
            this.especialidades = data;
        })
    }

    agregarDetalle() {
        if (this.diagnostico != null && this.tratamiento != null) {
            let det = new DetalleConsulta();
            det.diagnostico = this.diagnostico;
            det.tratamiento = this.tratamiento;
            this.detalleConsulta.push(det);

            this.diagnostico = null;
            this.tratamiento = null;
        }
    }

    removerDiagnostico(i: number) {
        this.detalleConsulta.splice(i, 1);
    }

    estadoBotonRegistrar(){
        return (this.especialidadSeleccionado === null || this.pacienteSeleccionado === null || this.medicoSeleccionado === null || this.detalleConsulta.length === 0 )
    }

    registrar(){
        debugger
     
       let consulta = new Consulta();
        consulta.paciente = this.pacienteSeleccionado;
        consulta.medico = this.medicoSeleccionado;
        consulta.especialidad = this.especialidadSeleccionado;
        consulta.numeroConsultorio = '1';
        consulta.detalleConsulta = this.detalleConsulta;
        //ISO DATE
        let tzoffset = (this.form.value['fecha']).getTimezoneOffset()*60000;
        let localIsoTimeDate = (new Date(Date.now() - tzoffset)).toISOString();
        consulta.fecha = localIsoTimeDate;

        let consultaExamenDTO = new ConsultaExamenDTO();
        consultaExamenDTO.consulta = consulta;
        consultaExamenDTO.listaExamen = this.examenesSeleccionados;
        this.consultaService.registrar(consultaExamenDTO).subscribe(()=>{
            this.snakBar.open('Consulta registrada', 'AVISO', { duration: 2000 });
            
            setTimeout(()=> {
                this.limpiarControles();
            }, 2000)
            
        })

       
    }


    agregarExamen() {
        if (this.examenSeleccionado != null) {
            debugger
            let cont: number = 0;
            for (let i = 0; i < this.examenesSeleccionados.length; i++) {
                let examen = this.examenesSeleccionados[i];
                if (examen.idExamen === this.examenSeleccionado.idExamen) {
                    cont++;
                    break;
                }
            }

            if (cont > 0) {
                this.mensaje = 'El exámen ya se ecuentra en la lista ';
                this.snakBar.open(this.mensaje, 'AVISO', { duration: 2000 });
            } else {
                let exa = new Examen();
                exa.idExamen = this.examenSeleccionado.idExamen;

                this.examenesSeleccionados.push(this.obtenerExamenOfLista(exa.idExamen));
            }

        } else {
            this.mensaje = 'Debe seleccionar un exámen ';
            this.snakBar.open(this.mensaje, 'AVISO', { duration: 2000 });
        }
    }

    obtenerExamenOfLista(idExamen: number) {
        debugger
        let examenAuxiliar = new Examen();
        this.examenes.map(function (e) {
            if (e.idExamen === idExamen) {
                examenAuxiliar = e;
            }
        });
        return examenAuxiliar;
    }

    removerExamen(index: number) {
        this.examenesSeleccionados.splice(index, 1);
    }

    limpiarControles(){
        this.detalleConsulta = [];
        this.examenesSeleccionados = [];
        this.diagnostico= null;
        this.tratamiento= null;
        this.especialidadSeleccionado =null;
        this.pacienteSeleccionado = null;
        this.medicoSeleccionado =null;
        this.examenSeleccionado = null;
        this.mensaje = '';
    }

}
