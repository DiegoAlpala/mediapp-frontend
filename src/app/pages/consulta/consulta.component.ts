import { ConsultaExamenDTO } from './../../_dto/consultaExamenDTO';
import { Consulta } from './../../_model/consulta';
import { logging } from 'protractor';
import { ConsultaService } from './../../_service/consulta.service';
import { ExamenService } from './../../_service/examen.service';
import { EspecialidadService } from './../../_service/especialidad.service';
import { MedicoService } from './../../_service/medico_service';
import { PacienteService } from './../../_service/paciente_service';
import { DetalleConsulta } from './../../_model/detalleConsulta';
import { Examen } from './../../_model/examen';
import { Medico } from './../../_model/medico';
import { Especialidad } from './../../_model/especialidad';
import { EspecialidadComponent } from './../especialidad/especialidad.component';
import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-consulta',
    templateUrl: './consulta.component.html',
    styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

    pacientes: Paciente[];
    medicos: Medico[];
    especialidades: Especialidad[];
    examenes: Examen[];

    maxFecha: Date = new Date();
    fechaSeleccionada: Date = new Date();

    diagnostico: string;
    tratamiento: string;
    mensaje: string;

    detalleConsulta: DetalleConsulta[] = [];
    examenesSeleccionados: Examen[] = [];

    idPacienteSeleccionado: number;
    idEspecialidadSeleccionado: number;
    idExamenSeleccionado: number;
    idMedicoSeleccionado: number;

    panelOpenState: boolean = false;

    constructor(
        private pacinteService: PacienteService,
        private medicoService: MedicoService,
        private especialidadService: EspecialidadService,
        private examenService: ExamenService,
        private consultaService: ConsultaService,
        private snakBar: MatSnackBar
    ) { }

    ngOnInit() {
        this.listarPacientes();
        this.listarMedicos();
        this.listarExamenes();
        this.listarEspecialidades();

    }

    listarPacientes() {
        this.pacinteService.listar().subscribe(data => {
            this.pacientes = data;
        })
    }

    listarMedicos() {
        this.medicoService.listar().subscribe(data => {
            this.medicos = data;
        })
    }

    listarExamenes() {
        this.examenService.listar().subscribe(data => {
            this.examenes = data;
        })
    }

    listarEspecialidades() {
        this.especialidadService.listar().subscribe(data => {
            this.especialidades = data;
        })
    }

    agregarDetalle() {
        if (this.diagnostico != null && this.tratamiento != null) {
            let det = new DetalleConsulta();
            det.diagnostico = this.diagnostico;
            det.tratamiento = this.tratamiento;
            this.detalleConsulta.push(det);

            this.diagnostico = null;
            this.tratamiento = null;
        }
    }

    removerDiagnostico(i: number) {
        this.detalleConsulta.splice(i, 1);
    }

    estadoBotonRegistrar(){
        return (this.idEspecialidadSeleccionado === 0 || this.idPacienteSeleccionado === 0 || this.idMedicoSeleccionado === 0 || this.detalleConsulta.length === 0 )
    }

    registrar(){
        debugger
       let medico = new Medico();
       medico.idMedico = this.idMedicoSeleccionado;
       let paciente = new Paciente();
       paciente.idPaciente = this.idPacienteSeleccionado;
       let especialidad = new Especialidad();
       especialidad.idEspecialidad = this.idEspecialidadSeleccionado;

       let consulta = new Consulta();
        consulta.paciente = paciente;
        consulta.medico = medico;
        consulta.especialidad = especialidad;
        consulta.numeroConsultorio = '1';
        consulta.detalleConsulta = this.detalleConsulta;
        //ISO DATE
        let tzoffset = (this.fechaSeleccionada).getTimezoneOffset()*60000;
        let localIsoTimeDate = (new Date(Date.now() - tzoffset)).toISOString();
        consulta.fecha = localIsoTimeDate;

        let consultaExamenDTO = new ConsultaExamenDTO();
        consultaExamenDTO.consulta = consulta;
        consultaExamenDTO.listaExamen = this.examenesSeleccionados;
        this.consultaService.registrar(consultaExamenDTO).subscribe(()=>{
            this.snakBar.open('Consulta registrada', 'AVISO', { duration: 2000 });
            
            setTimeout(()=> {
                this.limpiarControles();
            }, 2000)
            
        })

       
    }


    agregarExamen() {
        if (this.idExamenSeleccionado > 0) {
            debugger
            let cont: number = 0;
            for (let i = 0; i < this.examenesSeleccionados.length; i++) {
                let examen = this.examenesSeleccionados[i];
                if (examen.idExamen === this.idExamenSeleccionado) {
                    cont++;
                    break;
                }
            }

            if (cont > 0) {
                this.mensaje = 'El exámen ya se ecuentra en la lista ';
                this.snakBar.open(this.mensaje, 'AVISO', { duration: 2000 });
            } else {
                let exa = new Examen();
                exa.idExamen = this.idExamenSeleccionado;

                this.examenesSeleccionados.push(this.obtenerExamenOfLista(exa.idExamen));
            }

        } else {
            this.mensaje = 'Debe seleccionar un exámen ';
            this.snakBar.open(this.mensaje, 'AVISO', { duration: 2000 });
        }
    }

    obtenerExamenOfLista(idExamen: number) {
        debugger
        let examenAuxiliar = new Examen();
        this.examenes.map(function (e) {
            if (e.idExamen === idExamen) {
                examenAuxiliar = e;
            }
        });
        return examenAuxiliar;
    }

    removerExamen(index: number) {
        this.examenesSeleccionados.splice(index, 1);
    }

    limpiarControles(){
        this.detalleConsulta = [];
        this.examenesSeleccionados = [];
        this.diagnostico= null;
        this.tratamiento= null;
        this.idEspecialidadSeleccionado =0;
        this.idPacienteSeleccionado = 0;
        this.idMedicoSeleccionado =0;
        this.idExamenSeleccionado = 0;
        this.fechaSeleccionada = new Date();
        this.fechaSeleccionada.setHours(0);
        this.fechaSeleccionada.setMinutes(0);
        this.fechaSeleccionada.setSeconds(0);
        this.fechaSeleccionada.setMilliseconds(0);
        this.mensaje = '';
    }

}
