import { PacienteModalComponent } from './../paciente/paciente-modal/paciente-modal.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SignosService } from 'src/app/_service/signos.service';
import { MatSnackBar, MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { Signos } from 'src/app/_model/signos';
import { ActivatedRoute } from '@angular/router';
import { Paciente } from 'src/app/_model/paciente';

@Component({
    selector: 'app-signos',
    templateUrl: './signos.component.html',
    styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

    displayedColumns = ['idSignos', 'fecha', 'nombrePaciente', 'temperatura', 'pulso', 'ritmoRespiratorio', 'acciones'];
    dataSource: MatTableDataSource<Signos>;
    @ViewChild(MatSort, { static: true }) sort: MatSort //permite tener una referencia a nivel de codigo de lo que está en el html
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator

    constructor(private signosService: SignosService,
        private snackBar: MatSnackBar,
        public route: ActivatedRoute,
        ) { }

    ngOnInit() {
        this.signosService.signosCambio.subscribe(data => {
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        })

        this.signosService.mensajeCambio.subscribe(data => {
            this.snackBar.open(data, 'AVISO', {
                duration: 2000
            })
        })

        this.signosService.listar().subscribe(data => {
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator
        })
    }

    filtrar(e: string) {
        this.dataSource.filter = e.trim().toLowerCase();
    }

    eliminar(idSignos: number) {
        this.signosService.eliminar(idSignos).subscribe(() => {
            this.signosService.listar().subscribe(data => {
                this.signosService.signosCambio.next(data);
                this.signosService.mensajeCambio.next('Signos Eliminados');
            });
        });
    }

}
