import { switchMap } from 'rxjs/operators';
import { Signos } from './../../../_model/signos';
import { PacienteService } from './../../../_service/paciente_service';
import { Component, OnInit } from '@angular/core';
import { SignosService } from 'src/app/_service/signos.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Paciente } from 'src/app/_model/paciente';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { PacienteModalComponent } from '../../paciente/paciente-modal/paciente-modal.component';

@Component({
    selector: 'app-signos-edicion',
    templateUrl: './signos-edicion.component.html',
    styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

    form: FormGroup;
    id: number;
    edicion: boolean;
    signos: Signos;
    idPacienteSeleccionado : number;

    pacientes : Paciente[];

    constructor(
        private signosService: SignosService,
        private pacienteService: PacienteService,
        private route: ActivatedRoute,
        private router: Router,
        private dialog: MatDialog,
    ) { }

    ngOnInit() {
        this.signos = new Signos();
        this.form = new FormGroup({
            'id': new FormControl(0),
            'fecha': new FormControl(new Date()),
            'temperatura': new FormControl(''),
            'pulso': new FormControl(''),
            'ritmoRespiratorio': new FormControl('')
        });

        this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            this.edicion = params['id'] != null;
            this.initForm();
        });

        this.pacienteService.pacienteCambio.subscribe(data => {
            this.pacientes=data;
        });

        this.listarPacientes();
    }

    listarPacientes(){
        this.pacienteService.listar().subscribe(data =>{
            this.pacientes = data;
        })
    }

    initForm() {
        if (this.edicion) {
            this.signosService.listarPorId(this.id).subscribe(data => {
                this.form = new FormGroup({
                    'id': new FormControl(data.idSignos),
                    'fecha': new FormControl(data.fecha),
                    'temperatura': new FormControl(data.temperatura),
                    'pulso': new FormControl(data.pulso),
                    'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)
                });
                this.idPacienteSeleccionado= data.paciente.idPaciente;
            });
        }
    }

    operar() {
        debugger
        this.signos.idSignos = this.form.value['id'];
        this.signos.temperatura = this.form.value['temperatura'];
        this.signos.fecha = this.form.value['fecha'];
        this.signos.fecha = moment().format('YYYY-MM-DDTHH:mm:ss');
        this.signos.pulso = this.form.value['pulso'];
        this.signos.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
        let paciente = new Paciente();
        paciente.idPaciente= this.idPacienteSeleccionado;
        this.signos.paciente = paciente;

        if (this.signos != null && this.signos.idSignos > 0) {
            //MOdificar
            this.signosService.modificar(this.signos).pipe(switchMap(() => {
                return this.signosService.listar();
            })).subscribe(data => {
                this.signosService.signosCambio.next(data);
                this.signosService.mensajeCambio.next("SIGNOS MODIFICADOS");
            });
        } else {
            // Registrar
            this.signosService.registrar(this.signos).pipe(switchMap(() => {
                return this.signosService.listar();
            })).subscribe(data => {
                this.signosService.signosCambio.next(data);
                this.signosService.mensajeCambio.next("SIGNOS REGISTRADOS");
            });
        }

        this.router.navigate(['signos']);
    }

    abrirModal() {
        
        this.dialog.open(PacienteModalComponent, {
            width: '350px'
        })
    }

}
