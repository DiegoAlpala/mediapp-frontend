import { MenuService } from 'src/app/_service/menu.service';
import { Component, OnInit } from '@angular/core';
import { Menu } from './_model/menu';
import { LoginService } from './_service/login.service';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatDialog } from '@angular/material';
import { RefreshTokenComponent } from './pages/refresh-token/refresh-token.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'mediapp-frontend';

    menus : Menu[];

    constructor(private menuService : MenuService,
        public loginService : LoginService,
        private dialog: MatDialog){}

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.menuService.menuCambio.subscribe(data => {
            this.menus = data;
        });
    }

}


